<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

date_default_timezone_set("Europe/London"); // Default time zone. Set the time zone to your region. (see PHP manual)