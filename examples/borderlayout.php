<?php
require_once 'include.php';
require_once dirname(__DIR__) . '/src/autoinclude.php';

$webpage = new Sinevia\Html\Webpage();
$borderLayout = new \Sinevia\Html\BorderLayout();
$borderLayout->setParent($webpage)->setWidth("100%");

$buttonCenter = (new Sinevia\Html\Button())->setText("BUTTON CENTER");
$borderLayout->addCenter($buttonCenter);

$buttonTop = (new Sinevia\Html\Button())->setText("BUTTON TOP");
$borderLayout->addTop($buttonTop);

$buttonBottom = (new Sinevia\Html\Button())->setText("BUTTON BOTTOM");
$borderLayout->addBottom($buttonBottom);


$buttonLeft = (new Sinevia\Html\Button())->setText("BUTTON LEFT");
$borderLayout->addLeft($buttonLeft);


$buttonRight = (new Sinevia\Html\Button())->setText("BUTTON RIGHT");
$borderLayout->addRight($buttonRight);

$webpage->display();

