#SINEVIA HTML

A general purpose library for working with HTML

#SINEVIA SVG

A general purpose library for working with SVG

### Installation with Composer

```
#!json


    "repositories": [
        {
            "type": "vcs",
            "url": "https://sinevia@bitbucket.org/phplibrary/html.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/html": "dev-master"
    }
```


### Manual Installation

Download and install the source folder into a directory of your choosing. Include the autoinclude.php file from the source folder.


```
#!php

require_once '/src/autoinclude.php';
```

## Usage ##



```
#!php

<?php

require_once dirname(__DIR__).'/src/Sinevia/Html/autoinclude.php';

$webpage = new Sinevia\Html\Webpage;
echo $webpage->display();
```