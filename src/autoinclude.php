<?php
require_once dirname(__FILE__).'/Element.php'; // Must be first
require_once dirname(__FILE__).'/BorderLayout.php';
require_once dirname(__FILE__).'/Button.php';
require_once dirname(__FILE__).'/Div.php';
require_once dirname(__FILE__).'/Footer.php';
require_once dirname(__FILE__).'/Form.php';
require_once dirname(__FILE__).'/Header.php';
require_once dirname(__FILE__).'/Heading1.php';
require_once dirname(__FILE__).'/Hyperlink.php';
require_once dirname(__FILE__).'/Input.php';
require_once dirname(__FILE__).'/ListItem.php';
require_once dirname(__FILE__).'/OrderedList.php';
require_once dirname(__FILE__).'/Paragraph.php';
require_once dirname(__FILE__).'/Span.php';
require_once dirname(__FILE__).'/Tag.php';
require_once dirname(__FILE__).'/Textarea.php';
require_once dirname(__FILE__).'/UnorderedList.php';
require_once dirname(__FILE__).'/Webpage.php';