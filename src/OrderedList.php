<?php

// ========================================================================= //
// SINEVIA PUBLIC                                        http://sinevia.com  //
// ------------------------------------------------------------------------- //
// COPYRIGHT (c) 2016 Sinevia Ltd                        All rights resrved! //
// ------------------------------------------------------------------------- //
// LICENCE: All information contained herein is, and remains, property of    //
// Sinevia Ltd at all times.  Any intellectual and technical concepts        //
// are proprietary to Sinevia Ltd and may be covered by existing patents,    //
// patents in process, and are protected by trade secret or copyright law.   //
// Dissemination or reproduction of this information is strictly forbidden   //
// unless prior written permission is obtained from Sinevia Ltd per domain.  //
//===========================================================================//

namespace Sinevia\Html;

//============================= START OF CLASS ==============================//
// CLASS: OrderedList                                                        //
//===========================================================================//
class OrderedList extends Element {

    /**
     * The constructor of OrderedList
     * @construct
     */
    function __construct() {
        parent::__construct();
    }

    /**
     * Adds a new ListItem to the OrderedList
     * @param ListItem $item
     * @return OrderedList
     */
    function addItem($item) {
        $this->addChild($item);
        return $this;
    }

    function addChild($child) {
        if (is_object($child) && is_a($child, "Html_ListItem")) {
            $this->children[] = $child;
        } else {
            $list_item = new Html_ListItem();
            $list_item->addChild($child);
            $this->children[] = $list_item;
        }
        return $this;
    }

    /**
     * Returns the HTML representation of this OrderedList with its children.
     * @param compressed compresses the HTML, removing the new lines and indent
     * @param level the level of this widget
     * @return String html string
     */
    function toHtml($compressed = true, $level = 0) {
        if ($compressed == false) {
            $nl = "\n";
            $tab = "    ";
            $indent = str_pad("", ($level * 4));
        } else {
            $nl = "";
            $tab = "";
            $indent = "";
        }
        $html = $indent . '<ol' . $this->attributesToHtml() . $this->cssToHtml() . '>' . $nl;
        foreach ($this->children as $child) {
            if (is_object($child) && is_subclass_of($child, "Sinevia\Ui\Element")) {
                $html .= $child->toHtml($compressed, $level + 1) . $nl;
            } else {
                $html .= $indent . $tab . $child . $nl;
            }
        }
        $html .= $indent . '</ol>';
        return $html;
    }

    //=====================================================================//
    //  METHOD: toHtml                                                     //
    //========================== END OF METHOD ============================//
    //========================= START OF METHOD ===========================//
    //  METHOD: toXhtml                                                    //
    //=====================================================================//
    /**
     * Returns the XHTML representation of this OrderedList with its children.
     * @param compressed compresses the XHTML, removing the new lines and indent
     * @param level the level of this widget
     * @return String html string
     */
    function toXhtml($compressed = true, $level = 0) {
        if ($compressed == false) {
            $nl = "\n";
            $tab = "    ";
            $indent = str_pad("", ($level * 4));
        } else {
            $nl = "";
            $tab = "";
            $indent = "";
        }
        $html = $indent . '<ol' . $this->attributesToHtml() . $this->cssToHtml() . '>' . $nl;
        foreach ($this->children as $child) {
            if (is_object($child) && is_subclass_of($child, "Sinevia\Ui\Element")) {
                $html .= $child->toXhtml($compressed, $level + 1) . $nl;
            } else {
                $html .= $indent . $tab . $child . $nl;
            }
        }
        $html .= $indent . '</ol>';
        return $html;
    }

    //=====================================================================//
    //  METHOD: toXhtml                                                    //
    //========================== END OF METHOD ============================//
}

//===========================================================================//
// CLASS: OrderedList                                                        //
//============================== END OF CLASS ===============================//
?>